//
//  HomeTopCell.swift
//  newlywed
//
//  Created by Apple on 3/23/16.
//  Copyright © 2016 pagodalabs. All rights reserved.
//

import UIKit

class HomeTopCell: UITableViewCell {
    
    
    @IBOutlet weak var homeTopImage: UIImageView!
    
    @IBOutlet weak var homeLogo: UIImageView!
    
    @IBOutlet weak var homeLabel1: UILabel!

    @IBOutlet weak var homeLabel2: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
