//
//  Register.swift
//  newlywed
//
//  Created by Apple on 3/23/16.
//  Copyright © 2016 pagodalabs. All rights reserved.
//

import UIKit
import Alamofire

class Register: UIViewController {
    
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    var activityIndicator: UIActivityIndicatorView!
    var alert : Alert = Alert()
    var alertControl : UIAlertController!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .White)
        activityIndicator.hidesWhenStopped = true
        activityIndicator.center = self.view.center

    }

    @IBAction func registerButtonAction(sender: AnyObject)
    {
        let email = emailField.text! as String
        let password = passwordField.text! as String
        
        if emailField.text == "" || passwordField.text == ""
        {
//            alertControl = alert.showalert("Registration Failed", message: "Please Provide Both email and password", pop: false)
//            presentViewController(alertControl, animated: true, completion: nil)
            self.alert.showDefaultAlert("Registration Failed", message: "Please Provide Both Email and Password", pop: false, sender: self)
        }
        else
        {
            let parameters = ["email" : email , "password": password]
            activityIndicator.startAnimating()
            self.view.addSubview(activityIndicator)
            Alamofire.request(.POST, registerPost , parameters: parameters)
                .responseJSON { response in
            
                    switch response.result
                    {
                        case .Success:
                            if let JSON = response.result.value
                            {
                                    let status = JSON["success"] as! Bool
                                    if status == true
                                    {
//                                        self.alertControl = self.alert.showalert("Registration Success", message: "Registration Successful", pop: false)
//                                        self.presentViewController(self.alertControl, animated: true, completion: nil)
                                        self.alert.showDefaultAlert("Registration Success", message: "Registration Successful", pop: false, sender: self)
                                    }
                                    else
                                    {
//                                        self.alertControl = self.alert.showalert("Registration Failed", message: "Something Went Wrong", pop: false)
//                                        self.presentViewController(self.alertControl, animated: true, completion: nil)
                                        self.alert.showDefaultAlert("Registraiton Failed", message: "Something Went Wrong", pop: false, sender: self)
                                    }
                            }
                        
                    case .Failure(let error):
                        
//                                self.alertControl = self.alert.showalert("Registration Failed", message: error.localizedDescription, pop: false)
//                                self.presentViewController(self.alertControl, animated: true, completion: nil)
                        self.alert.showDefaultAlert("Registration Failed", message: error.localizedDescription, pop: false, sender: self)
                        
                    }
                    self.activityIndicator.stopAnimating()
            }
        }
    }

    @IBAction func endKeyboard(sender: AnyObject)
    {
        self.view.endEditing(true)
    }
}
