//
//  BlogsEntity+CoreDataProperties.swift
//  newlywed
//
//  Created by Apple on 7/2/16.
//  Copyright © 2016 pagodalabs. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension BlogsEntity {

    @NSManaged var id: String?
    @NSManaged var blog_title: String?
    @NSManaged var blog_description: String?
    @NSManaged var image_name: String?
    @NSManaged var modified_date: String?

}
