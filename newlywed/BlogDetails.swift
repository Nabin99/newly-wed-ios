//
//  BlogDetails.swift
//  newlywed
//
//  Created by Apple on 3/25/16.
//  Copyright © 2016 pagodalabs. All rights reserved.
//

import UIKit
import Alamofire

class BlogDetails: UIViewController {

    @IBOutlet weak var pageTitle: UILabel!
    @IBOutlet weak var date: UILabel!
    
    @IBOutlet weak var blogImage: UIImageView!
    @IBOutlet weak var blogDescription: UIWebView!
  
    var details: BlogsEntity!
    var sexplorationDetails : SexplorationDetailsEntity!
    var typestatus : String!
    var imagePath : String!
    var fromDB:Bool!;
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
     
        if typestatus == "blogs"
        {
            let imagename = details.image_name
            imagePath = "\(inspirationImageUrl)\(imagename!)"
            
            let imageURL : NSURL = NSURL(string: imagePath)!
            blogImage.image = nil
            
            if(!fromDB){
                
                Alamofire.request(.GET, imageURL)
                    .responseImage{ response in
                        if let image = response.result.value
                        {
                            self.blogImage.image = image
                        }
                        else
                        {
                            self.blogImage.image = UIImage(named: "no_image")
                        }
                }
            } else {
                
                let file = fileInDocumentsDirectory(imagename!)
                let fileManager = NSFileManager.defaultManager()
                if(fileManager.fileExistsAtPath(file)){
                    let imageContent = UIImage(contentsOfFile: file)
                    if(imageContent != nil){
                        self.blogImage.image = imageContent
                    } else {
                        self.blogImage.image = UIImage(named: "no_image")
                    }
                } else {
                    self.blogImage.image = UIImage(named: "no_image")
                }
            }
            
            pageTitle.text = details.blog_title
            date.text = details.modified_date
            blogDescription.loadHTMLString(details.blog_description!, baseURL: nil)
        }
        else
        {
            let imagename = sexplorationDetails.image_name
            imagePath = "\(maritalImageUrl)\(imagename!)"
            
            let imageURL : NSURL = NSURL(string: imagePath)!
            blogImage.image = nil
            
            if(!fromDB){
                
                Alamofire.request(.GET, imageURL)
                    .responseImage{ response in
                        if let image = response.result.value
                        {
                            self.blogImage.image = image
                        }
                        else
                        {
                            self.blogImage.image = UIImage(named: "no_image")
                        }
                }
            } else {
                
                let file = fileInDocumentsDirectory(imagename!)
                let fileManager = NSFileManager.defaultManager()
                if(fileManager.fileExistsAtPath(file)){
                    let imageContent = UIImage(contentsOfFile: file)
                    if(imageContent != nil){
                        self.blogImage.image = imageContent
                    } else {
                        self.blogImage.image = UIImage(named: "no_image")
                    }
                } else {
                    self.blogImage.image = UIImage(named: "no_image")
                }
            }
            
            pageTitle.text = sexplorationDetails.page_title
            date.text = sexplorationDetails.modified_date
            blogDescription.loadHTMLString(sexplorationDetails.page_description!, baseURL: nil)

        }
       
       
    }
    
    func getDocumentsDirectory() -> NSString{
        let paths = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }
    
    func fileInDocumentsDirectory(fileName:String) -> String {
        let fileName = getDocumentsDirectory().stringByAppendingPathComponent(fileName)
        return fileName
    }


   }
