//
//  VendorTypeEntity+CoreDataProperties.swift
//  newlywed
//
//  Created by Apple on 7/3/16.
//  Copyright © 2016 pagodalabs. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension VendorTypeEntity {

    @NSManaged var vendor_type_id: String?
    @NSManaged var vendor_type_name: String?
    @NSManaged var vendor_type_image_name: String?

}
