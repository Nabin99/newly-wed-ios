//
//  Notification.swift
//  newlywed
//
//  Created by Apple on 6/10/16.
//  Copyright © 2016 pagodalabs. All rights reserved.
//

import UIKit

class Notification: UIViewController {

    var notificationText:String!
    @IBOutlet weak var notificationTextView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        notificationTextView.text = notificationText
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backToHomeAction(sender: AnyObject) {
         let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let homeController: Home = mainStoryboard.instantiateViewControllerWithIdentifier("home") as! Home
        self.view.window?.rootViewController?.navigationController?.pushViewController(homeController, animated: true)
    }
}
