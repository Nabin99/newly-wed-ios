//
//  Blogs.swift
//  newlywed
//
//  Created by Apple on 3/24/16.
//  Copyright © 2016 pagodalabs. All rights reserved.
//

import UIKit
import Alamofire

class Blogs: UITableViewController {

    var activityIndicator : UIActivityIndicatorView!
    var arrData : NSArray = []
    var blogDetails : BlogsEntity!
    var alert : Alert = Alert()
    var alertControl : UIAlertController!
    var fromDB: Bool = false
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.tableView.registerNib(UINib(nibName: "CardViewCell", bundle: nil), forCellReuseIdentifier: "cardviewcell")
        self.refreshControl?.addTarget(self, action: #selector(Blogs.handleRefresh(_:)), forControlEvents: UIControlEvents.ValueChanged)
        
        activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .Gray)
        activityIndicator.hidesWhenStopped = true
        activityIndicator.center = self.view.center
        
        getBlogsDataFromDB()
        
        if(arrData.count > 0){
            fromDB = true
            self.tableView.reloadData()
        } else {
            fromDB = false
            getBlogsData();
        }
        

    }
    
    func getBlogsData()
    {
        if(!(refreshControl?.refreshing)!){
            activityIndicator.startAnimating()
        }
        self.view.addSubview(activityIndicator)
        Alamofire.request(.GET, jsonInspiration)
            .responseJSON{ response in
                switch response.result
                {
                    case .Success:
                        if let JSON = response.result.value
                        {
                            self.arrData = JSON.objectForKey("inspirations") as! NSArray
                            
                            let databaseHandler = DatabaseHandler(entityName: "BlogsEntity")
                            
                            databaseHandler.clearData()
                            
                            for(var count = 0; count < self.arrData.count ; count++){
                                let blogs = self.arrData[count] as! NSDictionary
                                let id = blogs["page_id"] as! String
                                let page_title = blogs["page_title"] as! String
                                let page_description = blogs["description"] as! String
                                let image_name = blogs["image_name"] as! String
                                let modified_date = blogs["modified_date"] as! String
                                databaseHandler.saveBlogs(id, blog_title: page_title, blog_description: page_description, image_name: image_name, modified_date: modified_date)
                            }
                            self.tableView.hidden = false
                            
                            self.getBlogsDataFromDB()
                            
                            self.tableView.reloadData()
                            
                            if((self.refreshControl?.refreshing)!){
                                self.refreshControl?.endRefreshing()
                            }
                        }
                        else
                        {
                            self.alertControl = self.alert.showalert("No Data", message: "No Data Found", pop: false)
                            self.presentViewController(self.alertControl, animated: true, completion: nil)
                        }
                    
                    case .Failure(let error):
                        
                    if(!(self.refreshControl?.refreshing)!){
                        self.alert.showDefaultAlert("Failure", message: error.localizedDescription, pop: true, sender: self)
                    } else {
                        self.refreshControl?.endRefreshing()
                    }
                    
                }
                
                if(!(self.refreshControl?.refreshing)!){
                    self.activityIndicator.stopAnimating()
                }
        }
    }

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        if (arrData.count > 0)
        {
            return 1
        }
        return 0
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        
        return arrData.count
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCellWithIdentifier("cardviewcell", forIndexPath: indexPath) as! CardViewCell

        cell.selectionStyle = .None
        let blogsData = arrData[indexPath.row] as! BlogsEntity
        //        BlogsEntity
        let imagename = blogsData.image_name
        let imagePath :String = "\(inspirationImageUrl)\(imagename!)"
        let imageURL : NSURL = NSURL(string: imagePath)!
        cell.cellLabel.text = blogsData.blog_title
        
        cell.cellActivityIndicator.startAnimating()
        cell.cellImage.image = nil
        
        if(!fromDB){
            print("DB Image: Alamofire Ma Gayo")
            Alamofire.request(.GET, imageURL)
                .responseImage{ response in
                    if let image = response.result.value
                    {
                        cell.cellImage.image = image
                        self.saveImage(image, image_name: blogsData.image_name!)
                    }
                    else
                    {
                        cell.cellImage.image = UIImage(named: "no_image")
                    }
                        cell.cellActivityIndicator.stopAnimating()
                        cell.cellActivityIndicator.hidesWhenStopped = true
            }
        } else {
            print("DB Image: From DB Ma Gayo")

                let file = fileInDocumentsDirectory(blogsData.image_name!)
                let fileManager = NSFileManager.defaultManager()
            if(fileManager.fileExistsAtPath(file)){
                let imageContent = UIImage(contentsOfFile: file)
                if(imageContent != nil){
                    cell.cellImage.image = imageContent
                } else {
                    cell.cellImage.image = UIImage(named: "no_image")
                }
            } else {
                cell.cellImage.image = UIImage(named: "no_image")
            }
        }
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        blogDetails = arrData[indexPath.row] as! BlogsEntity
        self.performSegueWithIdentifier("blogdetails", sender: nil)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?)
    {
        if segue.identifier == "blogdetails"
        {
            let blogObj : BlogDetails = segue.destinationViewController as! BlogDetails
            blogObj.details = blogDetails
            blogObj.typestatus = "blogs"
            blogObj.fromDB = true
        }
    }
    
    func getBlogsDataFromDB(){
        let blogsData = DatabaseHandler(entityName: "BlogsEntity")
        arrData = blogsData.getBlogsData()
    }
    
    func handleRefresh(refreshControl:UIRefreshControl){
        fromDB = false
        getBlogsData()
    }
    
    
    func saveImage(image:UIImage,image_name:String){
        if let data = UIImagePNGRepresentation(image){
            let filename = getDocumentsDirectory().stringByAppendingPathComponent(image_name)
            print("File Name: \(filename)")
            data.writeToFile(filename, atomically: true)
        }
    }
    
    func getDocumentsDirectory() -> NSString{
        let paths = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }
    
    func fileInDocumentsDirectory(fileName:String) -> String {
        let fileName = getDocumentsDirectory().stringByAppendingPathComponent(fileName)
        return fileName
    }

}
