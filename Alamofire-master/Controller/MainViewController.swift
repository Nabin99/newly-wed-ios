//
//  MainViewController.swift
//  BroadwayProject
//
//  Created by Apple on 2/4/16.
//  Copyright © 2016 Apple. All rights reserved.
//

import UIKit
import Alamofire

class MainViewController: UICollectionViewController {

    let arrHeaders : NSArray = ["Top Courses", "Upcoming Classes", "Student Testimonial", "Success Gallery", "Our Clients"]
    let arrSections : NSDictionary = ["Top Courses" : TOPCLASS, "Upcoming Classes" : UPCOMMING_CLASS, "Student Testimonial" : TESTIMONIAL, "Success Gallery" :  SUCCESS_GALLERY, "Our Clients" : CLIENT]
    var records: NSArray!
    var activityIndicator : UIActivityIndicatorView!
    var detailString : String!
    var imageLink:String!
    
    var dictDatas : NSMutableDictionary = ["Top Courses": [], "Upcoming Classes" : [], "Student Testimonial" : [], "Success Gallery" : [], "Our Clients" : []]
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        self.collectionView?.registerNib(UINib(nibName: "CustomCollectionCell1", bundle: nil), forCellWithReuseIdentifier: "cell1")
         self.collectionView?.registerNib(UINib(nibName: "CustomCollectionCell2", bundle: nil), forCellWithReuseIdentifier: "cell2")
        
        activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .Gray)
        activityIndicator.hidesWhenStopped = true
        activityIndicator.center = self.view.center

        for var count = 0; count < arrSections.count ; count++
        {
            let apiCall = arrHeaders[count] as! String
            let apiURL = arrSections[apiCall] as! String
            fetchData(apiURL, count:count)
        }
        
        //fetchData()
        
    }

    
    func fetchData(apiURL : String, count: Int){
        
        self.collectionView?.hidden = true
        activityIndicator.startAnimating()
        self.view.addSubview(activityIndicator)
        
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true

        Alamofire.request(.GET,apiURL,parameters:nil)
            .responseJSON { response in
                if let JSON = response.result.value {
                   
                    let dictKey = self.dictDatas.allKeys[count] as! String
                    self.dictDatas[dictKey] = JSON.objectForKey("records") as! NSArray
                  //print(self.arrSections.count)
                    //print(self.dictDatas["Top Courses"])
                    if count == self.arrSections.count - 1
                    {
                    
                        self.collectionView!.reloadData()
                        UIApplication.sharedApplication().networkActivityIndicatorVisible = false

                        self.activityIndicator.stopAnimating()
                        self.collectionView?.hidden = false
                    }
                }
        }

    }

 
    override func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        
        if dictDatas.count > 0
        {
            return dictDatas.allKeys.count
        }
        return 0
    }


    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        let arrSectionItem = (dictDatas.allValues[section] as! NSArray).count
         print("SEction Item :\(arrSectionItem)")
        return arrSectionItem
        
    }

    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier("cell1", forIndexPath: indexPath) as! CustomCollectionCell1
    
            let header: String = arrHeaders[indexPath.section] as! String
        
            let record = self.dictDatas[header] as! NSArray
        
        if record.count > 0
        {
            
        }
        
        print(record)
        /*cell.cellTitle.text = recordItem["title"] as? String
            
        
            let imageURL = "\(IMAGE_URL)\(recordItem["image"] as! String)"
            
            Alamofire.request(.GET, imageURL)
                .responseImage { response in
                    if let image = response.result.value {
                        cell.cellImage.image = image
                    }
            }
            // Synchronous Data Manipulation
            
            /*let data : NSData = NSData(contentsOfURL: NSURL(string: imageURL)!)!
            //let image = UIImage(data: data)
            cell.cellImage.image = image */
*/
            
            return cell
        }
    /*else{
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier("cell2", forIndexPath: indexPath) as! CustomCollectionCell2
           
            //cell.courseTitle.text = "asdfasdfasdf"
            return cell
        }*/
        
    
    
    func collectionView(collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
            
            
            if indexPath.section == 0
            {
                return CGSizeMake(152, 147)
            }
            
            if indexPath.section == 1
            {
                return CGSizeMake(collectionView.bounds.size.width, 92)
                
            }
            return CGSizeMake(135, 131)
    }

    
    override func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        
        let recordItem = records[indexPath.row] as! NSDictionary
        detailString = recordItem["detail"] as! String
        imageLink = "\(IMAGE_URL)\(recordItem["image"] as! String)"
        
        if indexPath.section == 0
        {
            self.performSegueWithIdentifier("details", sender: self)
        }
    }
    
    func collectionView(collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        referenceSizeForFooterInSection section: Int) -> CGSize
    {
        if section != 0
        {
            return CGSizeZero
        }
        else
        {
            return CGSizeMake(collectionView.bounds.size.width, 50)
        }
    }

    override func collectionView(collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, atIndexPath indexPath: NSIndexPath) -> UICollectionReusableView {
    
        var header: MyHeader?
        var footer: MyFooter?
        
       
        
        if kind == UICollectionElementKindSectionHeader
        {
            header = collectionView.dequeueReusableSupplementaryViewOfKind(kind, withReuseIdentifier: "header", forIndexPath: indexPath) as? MyHeader
            if indexPath.section == 0
            {
                header?.headerTitle.text = "Top Courses"
            }
            else if indexPath.section == 1
            {
                header?.headerTitle.text = "Upcoming Classes"
            }
                
        return header!
        }
        else
        {
            if indexPath.section == 0
            {
            footer = collectionView.dequeueReusableSupplementaryViewOfKind(kind, withReuseIdentifier: "footer", forIndexPath: indexPath) as? MyFooter
            
            return footer!
            }
            return UICollectionReusableView()
        }
        
        
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if segue.identifier == "details"
        {
        let detailObj : DetailViewController = segue.destinationViewController as! DetailViewController
            detailObj.courseDetails = detailString
            detailObj.imageURL = imageLink
        }
        
        }
    
    
    @IBAction func viewAllCourseAction(sender: UIButton) {
        
                   self.performSegueWithIdentifier("courses", sender: self)
       
    }
    
    
    @IBAction func enrollAction(sender: UIButton) {
        self.performSegueWithIdentifier("enroll", sender: self)
    }

    // MARK: UICollectionViewDelegate

    /*
    // Uncomment this method to specify if the specified item should be highlighted during tracking
    override func collectionView(collectionView: UICollectionView, shouldHighlightItemAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment this method to specify if the specified item should be selected
    override func collectionView(collectionView: UICollectionView, shouldSelectItemAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
    override func collectionView(collectionView: UICollectionView, shouldShowMenuForItemAtIndexPath indexPath: NSIndexPath) -> Bool {
        return false
    }

    override func collectionView(collectionView: UICollectionView, canPerformAction action: Selector, forItemAtIndexPath indexPath: NSIndexPath, withSender sender: AnyObject?) -> Bool {
        return false
    }

    override func collectionView(collectionView: UICollectionView, performAction action: Selector, forItemAtIndexPath indexPath: NSIndexPath, withSender sender: AnyObject?) {
    
    }
    */

}
