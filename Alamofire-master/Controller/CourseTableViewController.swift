//
//  CourseTableViewController.swift
//  BroadwayProject
//
//  Created by Apple on 2/6/16.
//  Copyright © 2016 Apple. All rights reserved.
//

import UIKit
import Alamofire

class CourseTableViewController: UITableViewController {

    var detailString: String!
    var courseArray : NSArray!
    var activityIndicator : UIActivityIndicatorView!
    var imageLink:String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
      
        activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .Gray)
        activityIndicator.hidesWhenStopped = true
        activityIndicator.center = self.view.center
        
        fetchData()

    }
    
    func fetchData(){
        
        activityIndicator.startAnimating()
        self.view.addSubview(activityIndicator)
        //self.view.backgroundColor = UIColor.grayColor()
        
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
        
        Alamofire.request(.GET,CATEGORY,parameters:nil)
            .responseJSON { response in
                if let JSON = response.result.value {
                    //print(JSON)
                    self.courseArray = JSON.objectForKey("records") as! NSArray
                    
                    self.tableView.reloadData()
                    UIApplication.sharedApplication().networkActivityIndicatorVisible = false
                    
                    self.activityIndicator.stopAnimating()
                }
        }
        
    }


     override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    
        if courseArray != nil
        {
        return courseArray.count
        }
        return 0
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath)

       let courseItem = courseArray[indexPath.row] as! NSDictionary
        cell.textLabel?.text = courseItem["title"] as? String
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        
        if courseArray != nil
        {
            let courseItem = courseArray[indexPath.row] as! NSDictionary
            detailString = courseItem["detail"] as! String
            imageLink = "\(IMAGE_URL)\(courseItem["image"] as! String)"
            self.performSegueWithIdentifier("details", sender: self)
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "details"
        {
            let detailObj : DetailViewController = segue.destinationViewController as! DetailViewController
            detailObj.courseDetails = detailString
            detailObj.imageURL = imageLink
        }
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
