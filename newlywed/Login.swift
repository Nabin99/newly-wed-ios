//
//  Login.swift
//  newlywed
//
//  Created by Apple on 3/23/16.
//  Copyright © 2016 pagodalabs. All rights reserved.
//

import UIKit
import Alamofire

class Login: UIViewController
{

    @IBOutlet weak var logo: UIImageView!
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    var alert : Alert = Alert()
    var alertControl : UIAlertController!
    
    var activityIndicator : UIActivityIndicatorView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        let textFieldIcon = UIImageView(frame: CGRectMake(0, 0, 22, 22))
        let passFieldIcon = UIImageView(frame: CGRectMake(0, 0, 20, 20))
        
        
        let textIcon = UIImage(named: "user")
        let passIcon = UIImage(named: "password")
        
        textFieldIcon.image = textIcon
        passFieldIcon.image = passIcon
        
        usernameTextField.leftViewMode = UITextFieldViewMode.Always
        passwordTextField.leftViewMode = UITextFieldViewMode.Always
        
        usernameTextField.leftView = textFieldIcon
        passwordTextField.leftView = passFieldIcon
        
        activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .White)
        activityIndicator.hidesWhenStopped = true
        activityIndicator.center = self.view.center

    }
    
    
    @IBAction func loginButtonAction(sender: UIButton)
    {
        let username = usernameTextField.text! as String
        let password = passwordTextField.text! as String
        
 
        if usernameTextField.text == "" || passwordTextField.text == ""
        {
            alertControl = alert.showalert("Login Failed",message: "Please Enter Both Username/Email and Password", pop: false)
            presentViewController(alertControl, animated: true, completion: nil)
        }
        else
        {
            let parameters = ["username" : username, "password": password]
            activityIndicator.startAnimating()
            self.view.addSubview(activityIndicator)
            Alamofire.request(.POST,loginPost,parameters: parameters)
                .responseJSON { response in
                    switch response.result
                    {
                    case .Success:
                            if let JSON = response.result.value
                            {
                                let status = JSON["success"] as! Bool
                                if status == true
                                {
                                    let jsonArr = (JSON["user_data"]) as! NSArray
                                    let jsonArray = jsonArr[0] as! NSDictionary
                                    let accessToken = jsonArray["access_token"] as! String
                                    let userArray = jsonArray["user"] as! NSDictionary
                                    let userid = userArray["id"] as! String
                                    let username = userArray["username"] as! String
                                    
                                    let prefs = NSUserDefaults.standardUserDefaults()
                                    prefs.setValue(userid, forKey: "user_id")
                                    prefs.setValue(username, forKey: "user_name")
                                    prefs.setValue(accessToken, forKey: "access_token")
                                    prefs.setValue("loggedin", forKey: "login")
                                    
                                    let HomeController = self.storyboard?.instantiateViewControllerWithIdentifier("home") as! Home
                                    self.navigationController?.pushViewController(HomeController, animated: true)
                                    
                                }
                                else if status == false
                                {
//                                    self.alertControl = self.alert.showalert("Login Failed", message: "Wrong Username or Password", pop: false)
//                                    self.presentViewController(self.alertControl, animated: true, completion: nil)
                                    self.alert.showDefaultAlert("Login Failed", message: "Wrong Username and Password", pop: false, sender: self)
                                }
                            }
                    case .Failure(let error):
//                            self.alertControl = self.alert.showalert("Login Failed", message: error.localizedDescription, pop: false)
//                            self.presentViewController(self.alertControl, animated: true, completion: nil)
                        self.alert.showDefaultAlert("Login Failed", message: error.localizedDescription, pop: false, sender: self)
                        
                    }
                self.activityIndicator.stopAnimating()
            
            }
        }
        
    }
    
    @IBAction func endKeyboard(sender: AnyObject)
    {
        self.view.endEditing(true)
    }
}
