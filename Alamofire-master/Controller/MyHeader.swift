//
//  MyHeader.swift
//  BroadwayProject
//
//  Created by Apple on 2/4/16.
//  Copyright © 2016 Apple. All rights reserved.
//

import UIKit

class MyHeader: UICollectionReusableView {
        
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var headerTitle: UILabel!
}
