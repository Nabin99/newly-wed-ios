//
//  SexplorationType.swift
//  newlywed
//
//  Created by Apple on 3/28/16.
//  Copyright © 2016 pagodalabs. All rights reserved.
//

import UIKit
import Alamofire

class SexplorationType: UITableViewController
{

    var sexplorationID : String!
    var activityIndicator : UIActivityIndicatorView!
    var sexplorationTypeData : NSArray = []
    var sexplorationDetails : SexplorationDetailsEntity!
    var alert : Alert = Alert()
    var alertControl : UIAlertController!
    var fromDB = false
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.tableView.registerNib(UINib(nibName: "CardViewCell", bundle: nil), forCellReuseIdentifier: "cardviewcell")
        self.refreshControl?.addTarget(self, action: #selector(Blogs.handleRefresh(_:)), forControlEvents: UIControlEvents.ValueChanged)

        activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .Gray)
        activityIndicator.hidesWhenStopped = true
        activityIndicator.center = self.view.center
        
        getSexplorationTypeDataFromDB()
        
        if(sexplorationTypeData.count > 0){
            fromDB = true
            self.tableView.reloadData()
        } else {
            fromDB = false
            getSexplorationtypeData()
        }
    }
    
    func getSexplorationtypeData()
    {
        if(!(refreshControl?.refreshing)!){
            activityIndicator.startAnimating()
            self.view.addSubview(activityIndicator)
        }
        
        let url =  jsonMarital + "\(sexplorationID)" + ".json"
        
        Alamofire.request(.GET,url)
            .responseJSON{ response in
                switch response.result
                {
                case .Success:
                    if let JSON = response.result.value
                    {
                        self.sexplorationTypeData = JSON["maritals"] as! NSArray
                        let databaseHandler = DatabaseHandler(entityName: "SexplorationDetailsEntity")
                        databaseHandler.clearSexplorationDetailData(self.sexplorationID)
                        
                        for var i=0; i<self.sexplorationTypeData.count; i++
                        {
                            let data = self.sexplorationTypeData[i] as! NSDictionary
                            let id = data["category"] as! String
                            let page_title = data["page_title"] as! String
                            let page_id = data["page_id"] as! String
                            let description = data["description"] as! String
                            let imageName = data["image_name"] as! String
                            let modifiedDate = data["modified_date"] as! String
                            databaseHandler.saveSexplorationDetails(id, pageId: page_id, pageTitle: page_title, description: description, imageName: imageName, modifiedDate: modifiedDate)
                        }
                        self.getSexplorationTypeDataFromDB()
                        self.tableView.reloadData()
                        
                        if((self.refreshControl?.refreshing)!){
                            self.refreshControl?.endRefreshing()
                        }

                    }
                    
                case .Failure(let error):
                    if(!(self.refreshControl?.refreshing)!){
                        self.alert.showDefaultAlert("Failure", message: error.localizedDescription, pop: true, sender: self)
                    } else {
                        self.refreshControl?.endRefreshing()
                    }

                    
                }
                if(!(self.refreshControl?.refreshing)!){
                    self.activityIndicator.stopAnimating()
                }
        }
    }

    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        if sexplorationTypeData.count > 0
        {
            return 1
        }
        return 0
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return sexplorationTypeData.count
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCellWithIdentifier("cardviewcell", forIndexPath: indexPath) as! CardViewCell
        cell.selectionStyle = .None
        
        let data = sexplorationTypeData[indexPath.row] as! SexplorationDetailsEntity
        let image = data.image_name! as String
        let imagePath = "\(maritalImageUrl)\(image)"
        
        let imageURL : NSURL = NSURL(string: imagePath)!

        
        cell.cellLabel.text = data.page_title! as String
        cell.cellActivityIndicator.startAnimating()
        if(!fromDB){
            cell.cellImage.image = nil
        Alamofire.request(.GET, imageURL)
            .responseImage{ response in
                if let image = response.result.value
                {
                    cell.cellImage.image = image
                    self.saveImage(image, image_name: data.image_name!)
                }
                else
                {
                    cell.cellImage.image = UIImage(named: "no_image")
                }
                cell.cellActivityIndicator.stopAnimating()
                cell.cellActivityIndicator.hidesWhenStopped = true
        }
        } else {
            let file = fileInDocumentsDirectory(data.image_name!)
            let fileManager = NSFileManager.defaultManager()
            if(fileManager.fileExistsAtPath(file)){
                let imageContent = UIImage(contentsOfFile: file)
                if(imageContent != nil){
                    cell.cellImage.image = imageContent
                } else {
                    cell.cellImage.image = UIImage(named: "no_image")
                }
            } else {
                cell.cellImage.image = UIImage(named: "no_image")
            }
        }

        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        sexplorationDetails = sexplorationTypeData[indexPath.row] as! SexplorationDetailsEntity
        self.performSegueWithIdentifier("sexplorationdetail", sender: nil)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?)
    {
        let sexploreObj : BlogDetails = segue.destinationViewController as! BlogDetails
        sexploreObj.sexplorationDetails = sexplorationDetails
        sexploreObj.typestatus = "sexploration"
        sexploreObj.fromDB = true
    }
    
    func getSexplorationTypeDataFromDB(){
        let sexplorationType = DatabaseHandler(entityName: "SexplorationDetailsEntity")
        sexplorationTypeData = sexplorationType.getSexplorationTypeData(sexplorationID)
    }
    
    func handleRefresh(refreshControl:UIRefreshControl){
        fromDB = false
        getSexplorationtypeData()
    }
    
    
    func saveImage(image:UIImage,image_name:String){
        if let data = UIImagePNGRepresentation(image){
            let filename = getDocumentsDirectory().stringByAppendingPathComponent(image_name)
            print("File Name: \(filename)")
            data.writeToFile(filename, atomically: true)
        }
    }
    
    func getDocumentsDirectory() -> NSString{
        let paths = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }
    
    func fileInDocumentsDirectory(fileName:String) -> String {
        let fileName = getDocumentsDirectory().stringByAppendingPathComponent(fileName)
        return fileName
    }


  }
