//
//  EnrollTableViewController.swift
//  BroadwayProject
//
//  Created by Apple on 2/7/16.
//  Copyright © 2016 Apple. All rights reserved.
//

import UIKit

class EnrollTableViewController: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        

    }

   
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
       
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 9
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        //let cell1 = tableView.dequeueReusableCellWithIdentifier("Cell1", forIndexPath: indexPath) as! EnrollCustom1
        //let cell2 = tableView.dequeueReusableCellWithIdentifier("Cell2", forIndexPath: indexPath) as! EnrollCustom2

       if indexPath.row == 0
       {
        let cell1 = tableView.dequeueReusableCellWithIdentifier("Cell1", forIndexPath: indexPath) as! EnrollCustom1
            cell1.labelText.text = "Name:"
            cell1.textfieldText.placeholder = "Name"
            return cell1
        }
        
       else if indexPath.row == 1
       {
        let cell1 = tableView.dequeueReusableCellWithIdentifier("Cell1", forIndexPath: indexPath) as! EnrollCustom1
            cell1.labelText.text = "Email *"
            cell1.textfieldText.placeholder = "Email"
            return cell1
       }
        
       else if indexPath.row == 2
       {
        let cell1 = tableView.dequeueReusableCellWithIdentifier("Cell1", forIndexPath: indexPath) as! EnrollCustom1
            cell1.labelText.text = "Mobile *"
            cell1.textfieldText.placeholder = "Mobile"
            cell1.textfieldText.keyboardType = UIKeyboardType.PhonePad
            return cell1
        }
        
       else if indexPath.row == 3
       {
        let cell1 = tableView.dequeueReusableCellWithIdentifier("Cell1", forIndexPath: indexPath) as! EnrollCustom1
            cell1.labelText.text = "Phone:"
            cell1.textfieldText.placeholder = "Phone"
            cell1.textfieldText.keyboardType = UIKeyboardType.PhonePad
            return cell1
       }
        
        else if indexPath.row == 4
       {
        
            let cell2 = tableView.dequeueReusableCellWithIdentifier("Cell2", forIndexPath: indexPath) as! EnrollCustom2
            cell2.labelText.text = "If Other Course"
            return cell2
        }
        
        else if indexPath.row == 5
       {
        
            let cell2 = tableView.dequeueReusableCellWithIdentifier("Cell2", forIndexPath: indexPath) as! EnrollCustom2
            cell2.labelText.text = "Your Information"
            return cell2
        }
        
        else if indexPath.row == 6
       {
            let cell3 = tableView.dequeueReusableCellWithIdentifier("Cell3", forIndexPath: indexPath) as! EnrollCustom3
            return cell3
        }

        else if indexPath.row == 7
       {
            let cell4 = tableView.dequeueReusableCellWithIdentifier("Cell4", forIndexPath: indexPath) as! EnrollCustom4
            return cell4
        }
        else
       {
            let cell5 = tableView.dequeueReusableCellWithIdentifier("Cell5", forIndexPath: indexPath) as! EnrollCustom5
            return cell5
        }
      
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if indexPath.row == 4 || indexPath.row == 5
        {
            return 130
        }
        else if indexPath.row == 8
        {
            return 150
        }
        return 66
    }
    
    @IBAction func EndKeyboard(sender: UITextField) {
        self.view.endEditing(true)
    }
    
    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 130
    }
    
   /* override func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        <#code#>
    }*/

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

