//
//  Sexploration.swift
//  newlywed
//
//  Created by Apple on 3/28/16.
//  Copyright © 2016 pagodalabs. All rights reserved.
//

import UIKit
import Alamofire


class Sexploration: UITableViewController
{
    var activityIndicator: UIActivityIndicatorView!
    var sexplorationData : NSArray = []
    var updatedSexplorationData : NSMutableArray = []
    var images : NSArray = ["pregnancy","sexual_and_reproductive_health","contraceptives","safe_abortion","councelling_and_services"]
    var sexplorationID : String!
    var alert : Alert = Alert()
    var alertControl : UIAlertController!
    var fromDB : Bool = false
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.refreshControl?.addTarget(self, action: #selector(Sexploration.handleRefresh(_:)), forControlEvents: UIControlEvents.ValueChanged)
        activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .Gray)
        activityIndicator.hidesWhenStopped = true
        activityIndicator.center = self.view.center
        
        getSexplorationDataFromDB();
        
        if(sexplorationData.count > 0){
            fromDB = true
            self.tableView.reloadData()
        } else {
            fromDB = false
            getSexplorationData()
        }

    }
    
    func getSexplorationData()
    {
        
        if(!(refreshControl?.refreshing)!){
            activityIndicator.startAnimating()
            self.view.addSubview(activityIndicator)
        }
        Alamofire.request(.GET,jsonSexploration)
            .responseJSON{ response in
                switch response.result
                {
                case .Success:
                    if let JSON = response.result.value
                    {
                        self.sexplorationData = JSON["categories"] as! NSArray
                        let databaseHandler = DatabaseHandler(entityName: "SexplorationEntity")
                        databaseHandler.clearData()
                        //count is started from  1 because we need to omit first element of data
                        print("Sexploration DAta: \(self.sexplorationData)")
                        for var i=1; i<self.sexplorationData.count; i++
                        {
                            let data = self.sexplorationData[i] as! NSDictionary
                            let id = data["id"] as! String
                            let name = data["name"] as! String
                            databaseHandler.saveSexploration(id, name: name)
                        }
                        self.getSexplorationDataFromDB()
                        self.tableView.reloadData()
                        if((self.refreshControl?.refreshing)!){
                            self.refreshControl?.endRefreshing()
                        }
                    }
                case .Failure(let error):
                    if(!(self.refreshControl?.refreshing)!){
                        self.alert.showDefaultAlert("Failure", message: error.localizedDescription, pop: true, sender: self)
                    } else {
                        self.refreshControl?.endRefreshing()
                    }

                }
                 if(!(self.refreshControl?.refreshing)!){
                        self.activityIndicator.stopAnimating()
                }
            }
    }

   
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        if sexplorationData.count > 0
        {
            return 1
        }
       
            return 0
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        
        return sexplorationData.count
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
    
        let cell = tableView.dequeueReusableCellWithIdentifier("sexplorationcell", forIndexPath: indexPath) as! sexplorationcell
        cell.selectionStyle = .None
        
        let data = sexplorationData[indexPath.row] as! SexplorationEntity
        cell.cellLabel.text = data.name! as String
        cell.cellImage.image = UIImage(named: (images[indexPath.row] as? String)!)
        return cell
      
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        let data = sexplorationData[indexPath.row] as! SexplorationEntity
        sexplorationID = data.id! as String
        self.performSegueWithIdentifier("sexplorationtype", sender: nil)
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 130
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?)
    {
        if segue.identifier == "sexplorationtype"
        {
            let sexploreObj : SexplorationType = segue.destinationViewController as! SexplorationType
            sexploreObj.sexplorationID = sexplorationID
        }
    }
    
    func getSexplorationDataFromDB(){
        let sexploration = DatabaseHandler(entityName: "SexplorationEntity")
        sexplorationData = sexploration.getSexplorationData()
    }
    
    func handleRefresh(refreshControl:UIRefreshControl){
        fromDB = false
        getSexplorationData()
    }
    

   }
