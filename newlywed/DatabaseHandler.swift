//
//  DatabaseHandler.swift
//  newlywed
//
//  Created by Apple on 6/23/16.
//  Copyright © 2016 pagodalabs. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class DatabaseHandler{
   
    let appDelegate : AppDelegate!
    let managedContext : NSManagedObjectContext!
    let entityName:String!
    
    init(entityName: String!){
        appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        managedContext = appDelegate.managedObjectContext
        self.entityName = entityName
    }
   
    func saveBlogs(id:String,blog_title:String,blog_description:String,image_name:String,modified_date:String){
        print("Entity Name: \(entityName)")
        
        let blogs = NSEntityDescription.insertNewObjectForEntityForName(entityName, inManagedObjectContext: managedContext) as! BlogsEntity
        
        blogs.setValue(id, forKey: "id")
        blogs.setValue(blog_title, forKey: "blog_title")
        blogs.setValue(blog_description, forKey: "blog_description")
        blogs.setValue(image_name, forKey: "image_name")
        blogs.setValue(modified_date, forKey: "modified_date")
        
        do{
            try managedContext.save()
        } catch let error as NSError{
            print("Couldn't Save \(error), \(error.userInfo)")
        }
        
    }
  
    func getBlogsData() -> [BlogsEntity]{
        var blogsData : [BlogsEntity] = []
        let fetchRequest = NSFetchRequest(entityName: entityName)
        do{
            let results = try managedContext.executeFetchRequest(fetchRequest) as! [BlogsEntity]
            blogsData = results
            return blogsData
        } catch let error as NSError {
            print("Couldn't Fetch \(error) \(error.userInfo)")
        }
        return blogsData
    }
    
    func saveSexploration(id:String,name:String){
        let sexploration = NSEntityDescription.insertNewObjectForEntityForName(entityName, inManagedObjectContext: managedContext) as! SexplorationEntity
        sexploration.id = id
        sexploration.name = name
        
        do{
            try managedContext.save()
        } catch let error as NSError{
            print("couldn't save \(error) , \(error.userInfo)")
        }
    }
    
    func getSexplorationData() -> [SexplorationEntity] {
        var results : [SexplorationEntity] = []
        let fetchRequest = NSFetchRequest(entityName: entityName)
        do{
            results = try managedContext.executeFetchRequest(fetchRequest) as! [SexplorationEntity]
            return results
        } catch let error as NSError {
            print("couldn't save \(error), \(error.userInfo)")
        }
        return results
    }
    
    func saveSexplorationDetails(id:String,pageId:String,pageTitle:String,description:String,imageName:String,modifiedDate:String){
        let sexplorationType = NSEntityDescription.insertNewObjectForEntityForName(entityName, inManagedObjectContext: managedContext) as! SexplorationDetailsEntity
        sexplorationType.parent_id = id
        sexplorationType.page_id = pageId
        sexplorationType.page_title = pageTitle
        sexplorationType.page_description = description
        sexplorationType.image_name = imageName
        sexplorationType.modified_date = modifiedDate
        do{
            try managedContext.save()
        } catch let error as NSError {
             print("couldn't save \(error) , \(error.userInfo)")
        }
        
    }
    
    func getSexplorationTypeData(id:String) -> [SexplorationDetailsEntity]{
        var results : [SexplorationDetailsEntity] = []
        let fetchRequest = NSFetchRequest(entityName: entityName)
        let predicate = NSPredicate(format: "%K == %@", "parent_id",id)
        fetchRequest.predicate = predicate
        do{
            results = try managedContext.executeFetchRequest(fetchRequest) as! [SexplorationDetailsEntity]
            return results
        } catch let error as NSError {
            print("Couldn't Fetch \(error), \(error.userInfo)")
        }
        return results

    }
    
    func saveVendorType(id:String,name:String,image_name:String){
        let vendorType = NSEntityDescription.insertNewObjectForEntityForName(entityName, inManagedObjectContext: managedContext) as! VendorTypeEntity
        vendorType.vendor_type_id = id
        vendorType.vendor_type_name = name
        vendorType.vendor_type_image_name = image_name
        do{
            try managedContext.save()
        } catch let error as NSError {
            print("Couldn't print \(error), \(error.userInfo)")
        }
    }
    
    
    func getVendorTypeData() -> [VendorTypeEntity] {
        var results : [VendorTypeEntity] = []
        let fetchRequest = NSFetchRequest(entityName: entityName)
        do{
            results = try managedContext.executeFetchRequest(fetchRequest) as! [VendorTypeEntity]
            return results
        } catch let error as NSError {
            print("Couldn't get \(error), \(error.userInfo)")
        }
        return results
    }
    
    func saveVendorsData(vendor_id:String,vendor_type_name:String,vendor_name:String,vendorTypeId:String,vendor_image:String,vendor_description:String,vendor_address:String,vendor_phone:String,state:String,city:String){
        let vendor = NSEntityDescription.insertNewObjectForEntityForName(entityName, inManagedObjectContext: managedContext) as! VendorsEntity
        vendor.vendor_id = vendor_id
        vendor.vendor_type = vendor_type_name
        vendor.vendor_name = vendor_name
        vendor.vendor_type_id = vendorTypeId
        vendor.vendor_image_name = vendor_image
        vendor.vendor_description = vendor_description
        vendor.vendor_address = vendor_address
        vendor.vendor_phone = vendor_phone
        vendor.vendor_state = state
        vendor.vendor_city = city
        
        do {
            try managedContext.save()
        } catch let error as NSError {
            print("Couldn't Save \(error), \(error.userInfo)")
        }
        
    }
    
    func getVendorsData(typeId:String) -> [VendorsEntity]{
        var results : [VendorsEntity] = []
        let fetchRequest = NSFetchRequest(entityName: entityName)
        let predicate = NSPredicate(format: "%K == %@", "vendor_type_id",typeId)
        fetchRequest.predicate = predicate
        do{
            results = try managedContext.executeFetchRequest(fetchRequest) as! [VendorsEntity]
            return results
        } catch let error as NSError {
            print("Couldn't Fetch \(error), \(error.userInfo)")
        }
        return results
    }
    
    func clearData(){
        let coord = appDelegate.persistentStoreCoordinator
        let fetchRequest = NSFetchRequest(entityName: entityName)
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
        do{
            try coord.executeRequest(deleteRequest, withContext: managedContext)
        } catch let error as NSError {
            print("Couldn't Delete \(error) \(error.userInfo)")
        }
    }
    
    func clearVendorsData(typeId:String){
        let coord = appDelegate.persistentStoreCoordinator
        let fetchRequest = NSFetchRequest(entityName: entityName)
        let predicate = NSPredicate(format: "%K == %@", "vendor_type_id",typeId)
        fetchRequest.predicate = predicate
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
        do{
            try coord.executeRequest(deleteRequest, withContext: managedContext)
        } catch let error as NSError {
            print("Couldn't Delete \(error) \(error.userInfo)")
        }

    }
    
    
    func clearSexplorationDetailData(id:String){
        let coord = appDelegate.persistentStoreCoordinator
        let fetchRequest = NSFetchRequest(entityName: entityName)
        let predicate = NSPredicate(format: "%K == %@", "parent_id",id)
        fetchRequest.predicate = predicate
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
        do{
            try coord.executeRequest(deleteRequest, withContext: managedContext)
        } catch let error as NSError {
            print("Couldn't Delete \(error) \(error.userInfo)")
        }
        
    }

}
