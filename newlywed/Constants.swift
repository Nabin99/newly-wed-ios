//
//  Constants.swift
//  newlywed
//
//  Created by Apple on 3/23/16.
//  Copyright © 2016 pagodalabs. All rights reserved.
//

import Foundation

let newlywedUrl = "http://www.pagodalabs.com.np/newlywed/"
let uploadUrl = newlywedUrl + "uploads/"
let saveTokenUrl = newlywedUrl + "gcm_user/api/ios_user?ios_token="
let inspirationImageUrl = uploadUrl + "inspiration/thumb/"
let tipImageUrl = uploadUrl + "page/"
let productImageUrl = uploadUrl + "product/"
let jsonInspiration = newlywedUrl + "inspiration/api/inspiration.json"
let jsonProduct = newlywedUrl + "product/api/product"
let jsonProductDetail = newlywedUrl + "product/api/detail?id="
let jsonTip = newlywedUrl + "tips/api/tips.json"
let jsonSexploration = newlywedUrl + "marital/api/marital.json"
let jsonMarital = newlywedUrl + "marital/api/marital?category_id="
let maritalImageUrl = uploadUrl + "page/thumb/"
let registerPost = newlywedUrl + "auth/api/register.json"
let loginPost = newlywedUrl + "auth/api/login.json"
let logout = newlywedUrl + "auth/api/logout.json"
let wishlistPost = newlywedUrl + "account/api/wishlist.json"
let wishListUrl = newlywedUrl + "/account/api/wishlist.json?userId="
let vendorImageUrl = newlywedUrl + "/uploads/vendors/"
let categoryImageUrl = uploadUrl + "category/"
let jsonVendorAppointment = newlywedUrl+"vendor/api/vendor.json"
let jsonVendor = newlywedUrl + "vendor/api/vendor?type="
let jsonVendorType = newlywedUrl + "vendor_type/api/vendor_type.json"
let vendorTypeImageUrl = newlywedUrl + "uploads/vendor_type/"
let jsonProductWithCategory = newlywedUrl + "/product/api/product?cat_name="
let jsonCalendar = newlywedUrl + "account/api/calendar?userId="
let jsonCheckList =  newlywedUrl + "account/api/checklist?userId="
let saveCheckList =  newlywedUrl + "account/api/checklist.json"
let jsonAppointments = newlywedUrl + "auth/api/appointments.json"
let addAppointment = newlywedUrl+"account/api/appointment.json"