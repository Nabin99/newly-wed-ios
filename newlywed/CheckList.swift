//
//  CheckList.swift
//  newlywed
//
//  Created by Apple on 4/6/16.
//  Copyright © 2016 pagodalabs. All rights reserved.
//

import UIKit
import Alamofire

class CheckList: UITableViewController {

    var activityIndicator : UIActivityIndicatorView!
    var checklistDict = NSDictionary()
    var myListArray : NSArray = []
    var checklistArray : NSArray = []
    var masterListArray : NSMutableArray = []
    var list : NSMutableArray!
    var mancount : Int = 0
    var checklistedArrayDict = [Dictionary<String,Int>]()
    var checkListFromUD :NSArray = []
    var listedArrUD = [Dictionary<String,String>]()
    var prefs = NSUserDefaults()
    var toStoreinUD : Bool = true
    var c=0;
    var alert : Alert = Alert()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        prefs = NSUserDefaults.standardUserDefaults()
       
        activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .Gray)
        activityIndicator.hidesWhenStopped = true
        activityIndicator.center = self.view.center
        getAllCheckList()

    }
    
    func getAllCheckList()
    {
        let prefs = NSUserDefaults.standardUserDefaults()
        let userID = prefs.valueForKey("user_id") as! String
        let accessToken = prefs.valueForKey("access_token") as! String
        
        let checklistURL = jsonCheckList + userID + "&access_token=" + accessToken + ".json"
        self.view.addSubview(activityIndicator)
        activityIndicator.startAnimating()
        Alamofire.request(.GET, checklistURL)
            .responseJSON{ response in
                switch(response.result)
                {
                case .Success:
                    if let JSON = response.result.value
                    {
                
                        self.checklistDict = JSON as! NSDictionary
                    
                        if self.checklistDict["success"] as! Int == 1
                        {
                            
                            self.checklistArray = self.checklistDict["master_checklist"] as! NSArray // every checklist available
                            self.myListArray = self.checklistDict["user_checklist"] as! NSArray     // checklist that has been checked by user
                            
                            if self.myListArray.count > 0
                            {
                                for var i in 0..<self.myListArray.count
                                {
                                    let checkId = self.myListArray[i] as! NSDictionary
                                    let dict = ["checklistID" : checkId["checklist_id"] as! String]
                                    self.listedArrUD.append(dict)           //get all the checklist that has been checked by user and append it in seperate array
                                    
                                }
                            }
                           prefs.setObject(self.listedArrUD, forKey: "CheckListFromUD")            //save the array to user Default
                            self.activityIndicator.stopAnimating()
                            self.tableView.reloadData()
                        }
                
                        print("Master CheckList:  \(self.checklistArray)");
                        print("Listed Array  \(self.listedArrUD)");
                        
                    }
                case .Failure(let error):
//                    let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .Alert)
//                    alert.addAction(UIAlertAction(title: "OK", style: .Default, handler: nil))
//                    self.presentViewController(alert, animated: true, completion: nil)
                    self.alert.showDefaultAlert("Error", message: error.localizedDescription, pop: true, sender: self)
                }
                self.activityIndicator.stopAnimating()
                
        }
    }

      override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        if checklistArray.count > 0
        {
            var count = 0;
            for (var i = 0 ; i < checklistArray.count ; i++)
            {
                let array = checklistArray[i] as! NSDictionary
                if array["parent_id"] as! String == "0"
                {
                    self.masterListArray.addObject(array["title"] as! String)
                    count++
                }
            }
            return count
        }
        return 0
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print(masterListArray)
        if checklistArray.count > 0
        {
            var count = 0;
            for (var i = 0 ; i < checklistArray.count ; i++)
            {
                let array = checklistArray[i] as! NSDictionary
                
                if masterListArray[section] as! String == array["parent_name"] as? String
                {
                    count++
                }
                
            }
            print("ROW COUNT: \(count)")
            return count
        }
        return 0
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath)
        cell.selectionStyle = .None
        checkListFromUD = prefs.objectForKey("CheckListFromUD") as! NSArray
        print("checked Array : \(checkListFromUD)")
        
        if checklistArray.count > 0
        {
            list = []
            for var i in 0..<checklistArray.count
            {
                
                let array = checklistArray[i] as! NSDictionary
                if masterListArray[indexPath.section] as! String == array["parent_name"] as? String
                {
                    list.addObject(checklistArray[i])
                }
            }
            let listDict = list[indexPath.row] as! NSDictionary
            
            if checkListFromUD.count > 0
            {
            
                for(var j = 0 ; j < checkListFromUD.count ; j++)
                {
                    let mylist = checkListFromUD[j] as! NSDictionary
                
                    if mylist["checklistID"] as? String == listDict["id"] as? String
                    {
                   
                            self.tableView.selectRowAtIndexPath(indexPath, animated: true, scrollPosition: UITableViewScrollPosition.None)
                            cell.setSelected(true, animated: true)
                            cell.accessoryType = .Checkmark
                        break
                    }
                    else
                    {
                        cell.selected = false
                        cell.setSelected(false, animated: true)
                        cell.accessoryType = .None
                    }
                }
            }
            else
            {
                cell.selected = false
                cell.setSelected(false, animated: true)
                cell.accessoryType = .None
            }
           
            cell.textLabel?.numberOfLines = 0
            cell.textLabel?.font = UIFont.systemFontOfSize(14)
            cell.textLabel?.text = listDict["title"] as? String
        
            return cell
        }
        print("LIST array: \(list)")
        return cell
    }
    
    
    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if masterListArray.count > 0
        {
            return self.masterListArray[section] as? String
        }
        return nil
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if let cell = tableView.cellForRowAtIndexPath(indexPath)
        {
            if cell.selected
            {
                list = []
                    for var i in 0..<checklistArray.count
                    {
                        
                        let array = checklistArray[i] as! NSDictionary
                        if masterListArray[indexPath.section] as? String == array["parent_name"] as? String
                        {
                            list.addObject(checklistArray[i])
                        }
                    }
                    let row = list[indexPath.row] as! NSDictionary
                    let checkRow = row["id"] as! String
                
                for var i in 0..<checkListFromUD.count
                {
                    let listChecked = checkListFromUD[i] as! NSDictionary
                    if checkRow != listChecked["checklistID"] as! String
                    {
                        toStoreinUD = true
                    }
                    else
                    {
                        toStoreinUD = false
                    }
                    
                }
                if toStoreinUD == true
                {
                    let dict = ["checklistID" : checkRow]
                    listedArrUD.append(dict)
                    prefs.setObject(self.listedArrUD, forKey: "CheckListFromUD")
                }
                self.tableView.selectRowAtIndexPath(indexPath, animated: true, scrollPosition: UITableViewScrollPosition.None)
                
                cell.setSelected(true, animated: true)
                cell.accessoryType = .Checkmark

            }
        }
        if let sr = tableView.indexPathsForSelectedRows
        {
            print("selected rows: \(sr)")
        }
    }
    
    override func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
        if let cell = tableView.cellForRowAtIndexPath(indexPath)
        {
            list = []
            for var i in 0..<checklistArray.count
            {
                
                let array = checklistArray[i] as! NSDictionary
                if masterListArray[indexPath.section] as! String == array["parent_name"] as? String
                {
                    list.addObject(checklistArray[i])
                }
            }
            let row = list[indexPath.row] as! NSDictionary
            let checkRow = row["id"] as! String
            
            for var j  in 0..<listedArrUD.count
            {
                let ad = listedArrUD[j] as? NSDictionary
                print(ad!["checklistID"])
                if ad!["checklistID"] as! String == checkRow
                {
                    listedArrUD.removeAtIndex(j)
                    print(listedArrUD)
                    prefs.setObject(listedArrUD, forKey: "CheckListFromUD")
                    cell.setSelected(false, animated: true)
                    cell.accessoryType = .None

                    break
                }
            }
            
        }
        if let sr = tableView.indexPathsForSelectedRows
        {
            print("didDeselectRowAtIndexPath selected rows:\(sr)")
        }
    }
    
    
    override func tableView(tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        let header = view as! UITableViewHeaderFooterView
        header.textLabel?.font = UIFont.systemFontOfSize(17)
    }
    
    override func viewDidDisappear(animated: Bool) {
       // prefs.removeObjectForKey("CheckListFromUD")
    }
    @IBAction func saveCheckListAction(sender: UIBarButtonItem) {
        
        var checklistArr = [Int]()
        for var i in 0..<listedArrUD.count
        {
            let arr = listedArrUD[i] as? NSDictionary
            let val = arr!["checklistID"] as! String
            checklistArr.append(Int(val)!)
        }
        
        let pref = NSUserDefaults.standardUserDefaults()
        let userID = pref.objectForKey("user_id") as! String
        let accessToken = pref.objectForKey("access_token") as! String
        
//        var stringArray = checklistArr.map(
//            {
//                (number: Int) -> String in
//                return String(number)
//        })
//        print(stringArray)
        let parameters : Dictionary<String,AnyObject> = ["userId" : userID , "access_token" : accessToken , "user_checklists" : checklistArr]
        print("PARAMETERS \(parameters)")
        
        activityIndicator.startAnimating()
        Alamofire.request(.POST, saveCheckList , parameters : parameters)
            .responseJSON { response in
        
                switch response.result
                {
                case .Success:
                    if let JSON = response.result.value
                        
                    {
                        print("CHECKLIST SUCCESS: \(JSON)");
                        if JSON["success"] as! Bool == true
                        {
                            let alert = UIAlertController(title: "Checklist Added", message: "Checklist Added Successfully", preferredStyle: .Alert)
                            alert.addAction(UIAlertAction(title: "OK", style: .Default, handler: nil))
                            self.presentViewController(alert, animated: true, completion: nil)
                        }
                        else{
                            let alert = UIAlertController(title: "Error", message: "Checklist Cannot be Added... Server Error", preferredStyle: .Alert)
                            alert.addAction(UIAlertAction(title: "OK", style: .Default, handler: nil))
                            self.presentViewController(alert, animated: true, completion: nil)
                        }
                    }
                case .Failure(let error):
                    let alert = UIAlertController(title: "No Internet Connection", message: error.localizedDescription, preferredStyle: .Alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .Default, handler: nil))
                    self.presentViewController(alert, animated: true, completion: nil)
                }
                self.activityIndicator.stopAnimating()
                
        }
        
    }
}
