//
//  AboutUs.swift
//  newlywed
//
//  Created by Apple on 4/6/16.
//  Copyright © 2016 pagodalabs. All rights reserved.
//

import UIKit

class AboutUs: UIViewController {

    @IBOutlet weak var aboutusScrollView: UIScrollView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var aboutusLabel: UILabel!
    @IBOutlet weak var aboutUsTextView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        aboutusScrollView.contentSize.height = 1000
        self.automaticallyAdjustsScrollViewInsets = false
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

   
}
