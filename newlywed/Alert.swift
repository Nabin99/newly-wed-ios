//
//  Alert.swift
//  newlywed
//
//  Created by Apple on 3/25/16.
//  Copyright © 2016 pagodalabs. All rights reserved.
//

import Foundation
import UIKit

class Alert : UIViewController
{
    func showalert(title:String, message:String, pop: Bool) -> UIAlertController
    {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .Alert)
        alert.addAction(UIAlertAction(title: "OK", style: .Default, handler: nil))
        return alert
    }
    
    func showalert1(title:String, message:String, pop: Bool , sender: UIViewController)
    {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .Alert)
        alert.addAction(UIAlertAction(title: "OK", style: .Default, handler: {action in self.alerthandler(pop , sender: sender)}))
        sender.presentViewController(alert, animated: true, completion: nil)
    }
    
    func showDefaultAlert(title:String, message:String, pop: Bool , sender: UIViewController)
    {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .Alert)
        alert.addAction(UIAlertAction(title: "OK", style: .Default, handler: {action in self.alerthandler(pop , sender: sender)}))
        sender.presentViewController(alert, animated: true, completion: nil)
    }
    
    func alerthandler(pop:Bool , sender: UIViewController)
    {
        if pop == true
        {
          sender.navigationController?.popViewControllerAnimated(true)
        }
    }

}