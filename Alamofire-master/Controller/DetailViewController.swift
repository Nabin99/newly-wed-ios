//
//  DetailViewController.swift
//  BroadwayProject
//
//  Created by Apple on 2/6/16.
//  Copyright © 2016 Apple. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {

    var imageURL:String!
    var courseDetails: String!
    @IBOutlet weak var webContent: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let width = webContent.frame.size.width
        
        let fullcontent:String = "<img src= \(imageURL) width=\(width) height=auto> </br> \(courseDetails)"
       webContent.loadHTMLString(fullcontent, baseURL: nil)
       
    }

}
