//
//  AddAppointmentPopView.swift
//  newlywed
//
//  Created by Apple on 4/4/16.
//  Copyright © 2016 pagodalabs. All rights reserved.
//

import UIKit
import Alamofire

class AddAppointmentPopView: UIViewController,UIPickerViewDelegate,UIPickerViewDataSource{

    @IBOutlet weak var vendorPicker: UIPickerView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var datePickerStart: UIDatePicker!
    @IBOutlet weak var datePickerEnd: UIDatePicker!
    @IBOutlet weak var appointmentTitle: UITextField!
    @IBOutlet weak var appointmentDesc: UITextView!
    var activityIndicator : UIActivityIndicatorView!
    var vendorValue : NSArray = []
    
    var dateFrom : String!
    var dateTo : String!
    var vendorID : String!
    var apptitle: String = ""
    var appdescription: String = ""
    var currentDate : String!
    var userID : String!
    var accessToken : String!
   
    override func viewDidLoad() {
        super.viewDidLoad()
        
        scrollView.contentSize.height = 700
        scrollView.directionalLockEnabled = true
        vendorPicker.delegate = self
        vendorPicker.dataSource = self
        getVendorValue()
        
        datePickerStart.addTarget(self, action: "dateStartChanged:", forControlEvents: UIControlEvents.ValueChanged)
    
        datePickerEnd.addTarget(self, action: "dateEndChanged:", forControlEvents: UIControlEvents.ValueChanged)
        
        currentDate = getCurrentDate()
        dateFrom = currentDate           // set initial dateFrom to currentDate
        dateTo = currentDate             // set initial dateTo to currentDate
        
        activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .Gray)
        activityIndicator.hidesWhenStopped = true
        activityIndicator.center = self.view.center
        
        let prefs = NSUserDefaults.standardUserDefaults()
        userID = prefs.valueForKey("user_id") as! String
        accessToken = prefs.valueForKey("access_token") as! String
       
    }
    
    func getCurrentDate() -> String
    {
        let todaysDate : NSDate = NSDate()
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        let date = dateFormatter.stringFromDate(todaysDate)
        return date
    }
    
    @IBAction func addAction(sender: AnyObject)
    {
        apptitle = appointmentTitle.text! as String
        appdescription = appointmentDesc.text as String
        
        if vendorID != nil && dateFrom != nil && dateTo != nil && apptitle != "" && appdescription != ""
        {
            print("\(vendorID)  \(dateFrom)  \(dateTo)  \(apptitle)  \(appdescription)")
            
            let parameters = ["userId" : userID , "access_token" : accessToken, "name" : apptitle, "description" : appdescription, "vendor_id" : vendorID , "from" : dateFrom , "to" : dateTo]
            activityIndicator.startAnimating()
            Alamofire.request(.POST,addAppointment,parameters: parameters)
                .responseJSON { response in
                    switch (response.result)
                    {
                    case .Success:
                        if let JSON = response.result.value
                        {
                            let success = JSON["success"] as! Int
                            if success == 1
                            {
                                let alert = UIAlertController(title: "Appointment Added ", message: "New Appointment has been added to your profile ", preferredStyle: .Alert)
                                alert.addAction(UIAlertAction(title: "OK", style: .Default, handler: {action in self.dismissController()}))
                                self.presentViewController(alert, animated: true, completion: nil)
                                
                            }
                            else
                            {
                                let alert = UIAlertController(title: "Adding Appointment Failed", message: "Something went Wrong", preferredStyle: .Alert)
                                alert.addAction(UIAlertAction(title: "OK", style: .Default, handler: nil))
                                self.presentViewController(alert, animated: true, completion: nil)
                            }
                        }
                    case .Failure (let error):
                        let alert = UIAlertController(title: "Adding Appointment Failed", message: error.localizedDescription, preferredStyle: .Alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .Default, handler: nil))
                        self.presentViewController(alert, animated: true, completion: nil)
                    }
            
            }
        }
        else
        {
            let alert = UIAlertController(title: "Add Appointment Failed", message: "Cannot add Appointment.. Please add all the fields", preferredStyle: .Alert)
            alert.addAction(UIAlertAction(title: "OK", style: .Default, handler: nil))
            presentViewController(alert, animated: true, completion: nil)
        }
        activityIndicator.stopAnimating()
    }
    
    func dismissController(){
        let controller = self.storyboard?.instantiateViewControllerWithIdentifier("calendar") as! Calendar
        self.navigationController?.popToViewController(controller, animated: true)
    }
    
    func getVendorValue()
    {
        Alamofire.request(.GET , jsonVendorAppointment)
            .responseJSON {response in
                    switch (response.result)
                    {
                    case .Success:
                            if let JSON = response.result.value
                            {
                                self.vendorValue = JSON["vendors"] as! NSArray
                                self.vendorPicker.reloadAllComponents()
                            }
                            break
                    case .Failure:
                        print ("Failure")
                        break
                    }
        }
    }
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        if vendorValue.count > 0
        {
            return 1
        }
        return 0
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {

        return  vendorValue.count
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        let vendorArray = vendorValue[row] as! NSDictionary
        if vendorID == nil
        {
            vendorID = vendorArray["id"] as? String
        }
        return vendorArray["vendor_name"] as? String
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int){
      
        let vendorArray = vendorValue[row] as! NSDictionary
        vendorID = vendorArray["id"] as! String
    }
    
    func dateStartChanged(datePicker: UIDatePicker)
    {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        let strDateFrom = dateFormatter.stringFromDate(datePicker.date)
        dateFrom = strDateFrom
    }
    
    func dateEndChanged(datePicker: UIDatePicker)
    {
        let dateFormatter = NSDateFormatter()
         dateFormatter.dateFormat = "yyyy-MM-dd"
        
        let strDateEnd = dateFormatter.stringFromDate(datePicker.date)
        dateTo = strDateEnd
    }
}
