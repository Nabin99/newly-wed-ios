//
//  HomeBottomCell.swift
//  newlywed
//
//  Created by Apple on 3/23/16.
//  Copyright © 2016 pagodalabs. All rights reserved.
//

import UIKit

class HomeBottomCell: UITableViewCell {
    
    
    @IBOutlet weak var bottomBackground: UIImageView!
    @IBOutlet weak var bottomLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
