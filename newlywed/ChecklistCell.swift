//
//  ChecklistCell.swift
//  newlywed
//
//  Created by Apple on 4/7/16.
//  Copyright © 2016 pagodalabs. All rights reserved.
//

import UIKit

class ChecklistCell: UITableViewCell {

    @IBOutlet weak var listLabel: UILabel!
    @IBOutlet weak var statusSwitch: UISwitch!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
