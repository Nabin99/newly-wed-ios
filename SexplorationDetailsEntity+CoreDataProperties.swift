//
//  SexplorationDetailsEntity+CoreDataProperties.swift
//  newlywed
//
//  Created by Apple on 7/5/16.
//  Copyright © 2016 pagodalabs. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension SexplorationDetailsEntity {

    @NSManaged var image_name: String?
    @NSManaged var modified_date: String?
    @NSManaged var page_description: String?
    @NSManaged var page_id: String?
    @NSManaged var page_title: String?
    @NSManaged var parent_id: String?

}
