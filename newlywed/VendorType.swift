//
//  VendorType.swift
//  newlywed
//
//  Created by Apple on 3/25/16.
//  Copyright © 2016 pagodalabs. All rights reserved.
//

import UIKit
import Alamofire
import CoreData

class VendorType: UITableViewController,UIPopoverPresentationControllerDelegate{

    
    var activityIndicator : UIActivityIndicatorView!
    var vendorTypesArray: NSArray = []
    var vendorname: String!
    var vendorTypeId:String!
    var alert : Alert = Alert()
    var alertControl : UIAlertController!
    var fromDB:Bool = false
    
    //var venName : NSArray = []
    //var venID : NSArray!
   

    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.tableView.registerNib(UINib(nibName: "CardViewCell", bundle: nil), forCellReuseIdentifier: "cardviewcell")
        self.refreshControl?.addTarget(self, action: #selector(VendorType.handleRefresh(_:)), forControlEvents: UIControlEvents.ValueChanged)
        activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .Gray)
        activityIndicator.hidesWhenStopped=true
        activityIndicator.center = self.view.center
       
        getVendorTypeDataFromDB()
        
        if(vendorTypesArray.count > 0){
            fromDB = true
            self.tableView.reloadData()
        } else {
            fromDB = false
            getVendorTypeData()
        }
       
    }
    
    
    func getVendorTypeData()
    {
        if(!(refreshControl?.refreshing)!){
            activityIndicator.startAnimating()
        }
        self.view.addSubview(activityIndicator)
        
        Alamofire.request(.GET, jsonVendorType)
            .responseJSON{response in
                switch response.result
                {
                case .Success:
                    if let JSON = response.result.value
                    {
                        self.vendorTypesArray = JSON["vendor_types"] as! NSArray
                        
                        let databaseHandler = DatabaseHandler(entityName: "VendorTypeEntity")
                        
                        databaseHandler.clearData()
                        
                        for (var count = 0; count<self.vendorTypesArray.count; count++)
                        {
                            let vendor = self.vendorTypesArray[count] as! NSDictionary
                            let vendortype = vendor["type"] as! String
                            let vendorid = vendor["id"] as! String
                            let vendorimage = vendor["image"] as! String
                        
                            databaseHandler.saveVendorType(vendorid, name: vendortype, image_name: vendorimage)
                        }
                        
                        self.getVendorTypeDataFromDB()
                        
                        self.tableView.reloadData()
                        
                        if((self.refreshControl?.refreshing)!){
                            self.refreshControl?.endRefreshing()
                        }
                    }
                    else
                    {
                        self.alertControl = self.alert.showalert("No Data", message: "No Data Found", pop: false)
                        self.presentViewController(self.alertControl, animated: true, completion: nil)
                    }
                    
                case .Failure(let error):
                    if(!(self.refreshControl?.refreshing)!){
                        self.alert.showDefaultAlert("Failed", message: error.localizedDescription, pop: true, sender: self);
                    } else {
                        self.refreshControl?.endRefreshing()
                    }
                    
                }
                if(!(self.refreshControl?.refreshing)!){
                        self.activityIndicator.stopAnimating()
                }
        }
    }
    
   
     override func numberOfSectionsInTableView(tableView: UITableView) -> Int
     {
        if vendorTypesArray.count > 0
            {
                return 1
            }
                return 0
      
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return vendorTypesArray.count
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cardviewcell", forIndexPath: indexPath) as! CardViewCell
        cell.selectionStyle = .None
        
            let vendor = vendorTypesArray[indexPath.row] as! VendorTypeEntity
            let vendortype = vendor.vendor_type_name!
            let vendorimage = vendor.vendor_type_image_name!
        
            let vendorImagePath = "\(vendorTypeImageUrl)\(vendorimage)"
            let imageURL : NSURL = NSURL(string: vendorImagePath)!
        
            cell.cellActivityIndicator.startAnimating()
            cell.cellLabel.text = vendortype
        if(!fromDB){
                cell.cellImage.image = nil
                Alamofire.request(.GET, imageURL)
                    .responseImage{ response in
                
                    if let image = response.result.value
                    {
                        cell.cellImage.image = image
                        self.saveImage(image, image_name: vendor.vendor_type_image_name!)
                    }
                    else
                    {
                        cell.cellImage.image = UIImage(named: "no_image")
                    }
                    cell.cellActivityIndicator.stopAnimating()
                    cell.cellActivityIndicator.hidesWhenStopped = true
            
            }
        } else {
            let file = fileInDocumentsDirectory(vendor.vendor_type_image_name!)
            let fileManager = NSFileManager.defaultManager()
            if(fileManager.fileExistsAtPath(file)){
                let imageContent = UIImage(contentsOfFile: file)
                if(imageContent != nil){
                    cell.cellImage.image = imageContent
                } else {
                    cell.cellImage.image = UIImage(named: "no_image")
                }
            } else {
                cell.cellImage.image = UIImage(named: "no_image")
            }
        }
        
        
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        let vendor = vendorTypesArray[indexPath.row] as! VendorTypeEntity
        vendorname = vendor.vendor_type_name! as String
        vendorTypeId = vendor.vendor_type_id! as String
        print(vendorname)
        self.performSegueWithIdentifier("vendors", sender: nil)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?)
    {
        if segue.identifier == "vendors"
        {
            let vendorObj : Vendors = segue.destinationViewController as! Vendors
            vendorObj.vendorname = vendorname
            vendorObj.vendortypeId = vendorTypeId
        }
    }
    
    func adaptivePresentationStyleForPresentationController(controller: UIPresentationController) -> UIModalPresentationStyle
    {
        return .None
    }
    
    func getVendorTypeDataFromDB(){
        let vendorTypeData = DatabaseHandler(entityName: "VendorTypeEntity")
        vendorTypesArray = vendorTypeData.getVendorTypeData()
    }
    
    func handleRefresh(refreshControl:UIRefreshControl){
        fromDB = false
        getVendorTypeData()
    }
    
    func saveImage(image:UIImage,image_name:String){
        if let data = UIImagePNGRepresentation(image){
            let filename = getDocumentsDirectory().stringByAppendingPathComponent(image_name)
            data.writeToFile(filename, atomically: true)
        }
    }
    
    func getDocumentsDirectory() -> NSString{
        let paths = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }
    
    func fileInDocumentsDirectory(fileName:String) -> String {
        let fileName = getDocumentsDirectory().stringByAppendingPathComponent(fileName)
        return fileName
    }

    
}
