//
//  Home.swift
//  newlywed
//
//  Created by Apple on 3/23/16.
//  Copyright © 2016 pagodalabs. All rights reserved.
//

import UIKit

class Home: UITableViewController{

    var loginstatus : Bool = false
    
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        let prefs = NSUserDefaults.standardUserDefaults()
        if prefs.stringForKey("user_id") != nil
        {
            loginstatus = true;
        }
        self.tableView.reloadData()
    }
    
    func menuItemClicked()
    {
        print("Clicked")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
       
    }
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
      
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
        return 6
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        if indexPath.row == 0
        {
            let cell = tableView.dequeueReusableCellWithIdentifier("hometopcell", forIndexPath: indexPath) as! HomeTopCell
            cell.homeTopImage.image = UIImage(named: "home")
            cell.homeLogo.image = UIImage(named: "logo")
            return cell
        }
        else if indexPath.row == 1
        {
            let cell = tableView.dequeueReusableCellWithIdentifier("homebottomcell", forIndexPath: indexPath) as! HomeBottomCell
            cell.bottomBackground.image = UIImage(named: "home_login")
            if loginstatus == false
            {
                cell.bottomLabel.text = "LOGIN"
            }
            else
            {
                cell.bottomLabel.text = "CALENDAR"
            }
        }
        else if indexPath.row == 2
        {
            let cell = tableView.dequeueReusableCellWithIdentifier("homebottomcell", forIndexPath: indexPath) as! HomeBottomCell
            cell.bottomBackground.image = UIImage(named: "home_register")
            
            if loginstatus == false
            {
                cell.bottomLabel.text = "REGISTER"
            }
            else
            {
                cell.bottomLabel.text = "CHECK LIST"
            }
        }
        else if indexPath.row == 3
        {
            let cell = tableView.dequeueReusableCellWithIdentifier("homebottomcell", forIndexPath: indexPath) as! HomeBottomCell
            cell.bottomBackground.image = UIImage(named: "home_blog")
            cell.bottomLabel.text = "BLOGS"
        }
        else if indexPath.row == 4
        {
            let cell = tableView.dequeueReusableCellWithIdentifier("homebottomcell", forIndexPath: indexPath) as! HomeBottomCell
            cell.bottomBackground.image = UIImage(named: "home_sexploration")
            cell.bottomLabel.text = "SEXPLORATION"
        }
        else if indexPath.row == 5
        {
            let cell = tableView.dequeueReusableCellWithIdentifier("homebottomcell", forIndexPath: indexPath) as! HomeBottomCell
            cell.bottomBackground.image = UIImage(named: "home_vendors")
            cell.bottomLabel.text = "VENDORS"
        }

        
        
            let cell = tableView.dequeueReusableCellWithIdentifier("homebottomcell", forIndexPath: indexPath) as! HomeBottomCell
            return cell
        
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        if indexPath.row == 0
        {
            return 200
            
        }
        else
        {
            return 100
        }
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        
        
        
        if indexPath.row == 1
        {
           if loginstatus == false
           {
                self.performSegueWithIdentifier("login", sender: nil)
           }
           else
           {
                self.performSegueWithIdentifier("calendar", sender: nil)
           }
          
        }
        else if indexPath.row == 2
        {
            if loginstatus == false
            {
                self.performSegueWithIdentifier("register", sender: nil)
            }
            else
            {
                self.performSegueWithIdentifier("checklist", sender: nil)
            }
        
        }
        else if indexPath.row == 3
        {
            self.performSegueWithIdentifier("blogs", sender: nil)
        }
        
        else if indexPath.row == 4
        {
            self.performSegueWithIdentifier("sexploration", sender: nil)
        }
        
        else if indexPath.row == 5
        {
            self.performSegueWithIdentifier("vendortype", sender: nil)
        }
        
    }
    
    override func viewDidAppear(animated: Bool)
    {
        self.tableView.reloadData()
    }
    
    override func viewWillAppear(animated: Bool)
    {
        super.viewWillAppear(true)
        self.navigationController?.navigationBarHidden = true
    }
    
    override func viewWillDisappear(animated: Bool)
    {
        super.viewWillDisappear(true)
        self.navigationController?.navigationBarHidden = false
    }
    
  
    @IBAction func menuAction(sender: AnyObject) {
        
        let actionsheet = UIAlertController(title: "Menu", message: "Choose an option", preferredStyle: .ActionSheet)
        
        let homeAction = UIAlertAction(title: "Home", style: .Default, handler: { (alert: UIAlertAction!) -> Void in
        
                print("Home")
        })
        
        let aboutAction = UIAlertAction(title: "About Us", style: .Default, handler: {(alert: UIAlertAction!) -> Void in
                self.performSegueWithIdentifier("aboutus", sender: nil)
        })
        
        let logoutAction = UIAlertAction(title: "Logout", style: .Default, handler: {(alert: UIAlertAction!) -> Void in
            self.showLogoutAlert()
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: nil)
        
        actionsheet.addAction(homeAction)
        actionsheet.addAction(aboutAction)
        actionsheet.addAction(cancelAction)
        
        if loginstatus == true{
            actionsheet.addAction(logoutAction)
        }
        presentViewController(actionsheet, animated: true, completion: nil)
    }
    
    func showLogoutAlert()
    {
        let alert = UIAlertController(title: "Log Out", message: "Are you sure you want to logout?", preferredStyle: .Alert)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: nil)
        
        let logoutAction = UIAlertAction(title: "Logout", style: .Default, handler: {(alert: UIAlertAction!) -> Void in
        
            let prefs = NSUserDefaults.standardUserDefaults()
            prefs.removeObjectForKey("user_id")
            let HomeController = self.storyboard?.instantiateViewControllerWithIdentifier("home") as! Home
            self.navigationController?.pushViewController(HomeController, animated: true)
        
        })
        
        alert.addAction(cancelAction)
        alert.addAction(logoutAction)
        presentViewController(alert, animated: true, completion: nil)
    }
}
