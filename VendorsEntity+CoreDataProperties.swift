//
//  VendorsEntity+CoreDataProperties.swift
//  newlywed
//
//  Created by Apple on 7/3/16.
//  Copyright © 2016 pagodalabs. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension VendorsEntity {

    @NSManaged var vendor_type_id: String?
    @NSManaged var vendor_type: String?
    @NSManaged var vendor_state: String?
    @NSManaged var vendor_phone: String?
    @NSManaged var vendor_name: String?
    @NSManaged var vendor_image_name: String?
    @NSManaged var vendor_id: String?
    @NSManaged var vendor_description: String?
    @NSManaged var vendor_city: String?
    @NSManaged var vendor_address: String?

}
