//
//  VendorsDetail.swift
//  newlywed
//
//  Created by Apple on 3/25/16.
//  Copyright © 2016 pagodalabs. All rights reserved.
//

import UIKit
import Alamofire

class VendorsDetail: UIViewController {

    @IBOutlet weak var vendorImageView: UIImageView!
    @IBOutlet weak var vendorName: UILabel!
    @IBOutlet weak var vendorAddress: UILabel!
    @IBOutlet weak var vendorDescription: UIWebView!
    
    @IBOutlet weak var vendorPhone: UILabel!
    var vendorDetail : VendorsEntity!
    var fromDB:Bool!
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        let imagename = vendorDetail.vendor_image_name!
        let imagePath :String = "\(vendorImageUrl)\(imagename)"
        
        let imageURL : NSURL = NSURL(string: imagePath)!
        //vendorImageView.image = nil
        print("Image File: \(fromDB)")

        if(!fromDB){
            print("Image File: Alamofire Exists")

            Alamofire.request(.GET, imageURL)
                .responseImage{ response in
                    if let image = response.result.value
                    {
                        self.vendorImageView.image = image
                    }
                    else
                    {
                        self.vendorImageView.image = UIImage(named: "no_image")
                    }
            }
        } else {
            let file = fileInDocumentsDirectory(imagename)
            let fileManager = NSFileManager.defaultManager()
            if(fileManager.fileExistsAtPath(file)){
                let imageContent = UIImage(contentsOfFile: file)
                if(imageContent != nil){
                    self.vendorImageView.image = imageContent
                } else {
                    self.vendorImageView.image = UIImage(named: "no_image")
                }
            } else {
                print("Image File: Image Doesnt Exists")

            }
        }
        
        vendorName.text = vendorDetail.vendor_name!
        vendorPhone.text = vendorDetail.vendor_phone!
        vendorAddress.text = vendorDetail.vendor_address!
        vendorDescription.loadHTMLString(vendorDetail.vendor_description! as String, baseURL: nil)
        
    }
    
    func getDocumentsDirectory() -> NSString{
        let paths = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }
    
    func fileInDocumentsDirectory(fileName:String) -> String {
        let fileName = getDocumentsDirectory().stringByAppendingPathComponent(fileName)
        return fileName
    }


   }
