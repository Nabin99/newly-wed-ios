//
//  Calendar.swift
//  newlywed
//
//  Created by Apple on 4/1/16.
//  Copyright © 2016 pagodalabs. All rights reserved.
//

import UIKit
import Alamofire

class Calendar: UIViewController,UIPopoverPresentationControllerDelegate,UITableViewDelegate,UITableViewDataSource{

    @IBOutlet weak var datePicker: UIDatePicker!
    var label: String!
    var userID : String!
    var accessToken : String!
    var activityIndicator : UIActivityIndicatorView!
    var appointmentsArray = NSArray()
    var currentDate : String!
    var arr : NSMutableArray = []
     var alert : Alert = Alert()
    
    @IBOutlet weak var appointmentTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        appointmentTableView.bounces = false
        datePicker.addTarget(self, action: "datePickerChanged:", forControlEvents: UIControlEvents.ValueChanged)
        
        let prefs = NSUserDefaults.standardUserDefaults()
        userID = prefs.valueForKey("user_id") as! String
        accessToken = prefs.valueForKey("access_token") as! String
        
        activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .Gray)
        activityIndicator.hidesWhenStopped = true
        activityIndicator.center = self.view.center
    
        currentDate = getCurrentDate()
        getAllAppointments()
        
        
        
        
    }
    
    func getAllAppointments()
    {
        let appointmentsURL = jsonCalendar + userID + "&access_token=" + accessToken + ".json"
        
        self.view.addSubview(activityIndicator)
        activityIndicator.startAnimating()
        
        Alamofire.request(.GET, appointmentsURL)
            .responseJSON{ response in
                switch(response.result)
                {
                case .Success:
                    if let JSON = response.result.value
                    {
                        self.appointmentsArray = JSON["calendar"] as! NSArray
                        self.getRequiredAppointments(self.appointmentsArray,date : self.currentDate)
                        
                        self.getRequiredAppointments(self.appointmentsArray, date : self.currentDate)
                    }
                    
                case .Failure(let error):
//                    let alert = UIAlertController(title: "No Internet Connection", message: error.localizedDescription, preferredStyle: .Alert)
//                    alert.addAction(UIAlertAction(title: "OK", style: .Default, handler: nil))
//                    self.presentViewController(alert, animated: true, completion: nil)
                    self.alert.showDefaultAlert("Error", message: error.localizedDescription, pop: false, sender: self)
                    
                }
                self.activityIndicator.stopAnimating()
                
        }

    }
    
    func getCurrentDate() -> String
    {
        let todaysDate : NSDate = NSDate()
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        let date = dateFormatter.stringFromDate(todaysDate)
        return date
    }

    
    func datePickerChanged(datePicker: UIDatePicker)
    {
        let dateformatter = NSDateFormatter()
        dateformatter.dateFormat = "yyyy-MM-dd"
        
        let strDate = dateformatter.stringFromDate(datePicker.date)
        getRequiredAppointments(appointmentsArray,date : strDate)
        
    }
    
    
    func getRequiredAppointments(appointmentsArray : NSArray , date : String)
    {
        arr = []
        
        for (var i = 0 ; i < appointmentsArray.count ; i++)
        {
            let appoint = appointmentsArray[i] as! NSDictionary
            if date == appoint["from"] as! String
            {
                arr.addObject(appointmentsArray[i])
            }
        }
        
      appointmentTableView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func addAppointmentAction(sender: AnyObject)
    {
        self.performSegueWithIdentifier("addappointment", sender: self)
        
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "addappointment"
        {
//            let vc = segue.destinationViewController
//            let controller = vc.popoverPresentationController
//            if controller != nil{
//                controller?.delegate = self
//            }
            
        }
    }
    
    func adaptivePresentationStyleForPresentationController(controller: UIPresentationController) -> UIModalPresentationStyle {
        return .None
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
            return 1
     
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       if arr.count > 0
        {
            return arr.count
        }
        return 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("appointmentcell", forIndexPath: indexPath) as! AppointmentCell
        if arr.count > 0
        {
            let appointArray = arr[indexPath.row] as! NSDictionary
            cell.vendorName.text = appointArray["vendor_name"] as? String
            cell.eventTitle.text = appointArray["title"] as? String
            cell.eventDescription.text = appointArray["description"] as? String
            return cell
        }
        else
        {
            cell.vendorName.text = "No Appointments made for this day"
            cell.eventTitle.text = ""
            cell.eventDescription.text = ""
            return cell
        }
    }
    
}
