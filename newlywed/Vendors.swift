//
//  Vendors.swift
//  newlywed
//
//  Created by Apple on 3/25/16.
//  Copyright © 2016 pagodalabs. All rights reserved.
//

import UIKit
import Alamofire

class Vendors: UITableViewController {

    var activityIndicator : UIActivityIndicatorView!
    var vendorname:String!
    var vendortypeId:String!
    var vendorsArray: NSArray = []
    var vendorDetail : VendorsEntity!
    var alert : Alert = Alert()
    var alertControl : UIAlertController!
    var fromDB:Bool = false
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.tableView.registerNib(UINib(nibName: "CardViewCell", bundle: nil), forCellReuseIdentifier: "cardviewcell")
        self.refreshControl?.addTarget(self, action: #selector(Vendors.handleRefresh(_:)), forControlEvents: UIControlEvents.ValueChanged)
        activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .Gray)
        activityIndicator.hidesWhenStopped = true
        activityIndicator.center = self.view.center

        getVendorsDataFromDB()
        
        if(vendorsArray.count > 0){
            fromDB = true
            self.tableView.reloadData()
        } else {
            fromDB = false
            getVendorsData()
        }
    }
    
    func getVendorsData()
    {
        let url = jsonVendor + vendorname + ".json"
        let urlwithoutspace = url.stringByReplacingOccurrencesOfString(" ", withString: "")
        
        if(!(self.refreshControl?.refreshing)!){
            self.view.addSubview(activityIndicator)
            activityIndicator.startAnimating()
        }
        
        Alamofire.request(.GET, urlwithoutspace)
            .responseJSON{ response in
                switch response.result
                {
                case .Success:
                    if let JSON = response.result.value
                    {
                        self.vendorsArray = JSON["vendors"] as! NSArray
                        print("Vendors Data: \(self.vendorsArray), \(self.vendorsArray.count)")
                        
                        if(self.vendorsArray.count > 0){
                        let databaseHandler = DatabaseHandler(entityName: "VendorsEntity")
                        
                        databaseHandler.clearVendorsData(self.vendortypeId)
                        
                        for(var i = 0 ; i < self.vendorsArray.count ; i++){
                            print("Vendors Count: \(self.vendorsArray.count)")
                            let vendors = self.vendorsArray[i] as! NSDictionary
                            let id = vendors["id"] as! String
                            let vendor_name = vendors["vendor_name"] as! String
                            let vendor_type_name = self.vendorname
                            let vendor_type_id = self.vendortypeId
                            let vendor_image = vendors["vendor_image"] as! String
                            let vendor_description = vendors["description"] as! String
                            let vendor_address = vendors["address"] as! String
                            let vendor_phone = vendors["phone"] as! String
                            let vendor_state = vendors["state"] as! String
                            let vendor_city = vendors["city"] as! String
                            
                            databaseHandler.saveVendorsData(id, vendor_type_name: vendor_type_name, vendor_name: vendor_name, vendorTypeId: vendor_type_id, vendor_image: vendor_image, vendor_description: vendor_description, vendor_address: vendor_address, vendor_phone: vendor_phone, state: vendor_state, city: vendor_city)
                            
                            }
                            self.getVendorsDataFromDB()
                            
                            self.tableView.reloadData()
                            
                            if((self.refreshControl?.refreshing)!){
                                self.refreshControl?.endRefreshing()
                            }
                        } else {
                            print("No Data Found")
                            self.alert.showDefaultAlert("No Vendors", message: "No Data found for this vendor", pop: true, sender: self)
                        }

                }
            
                case .Failure(let error):
                    if(!(self.refreshControl?.refreshing)!){
                        self.alert.showDefaultAlert("Failure", message: error.localizedDescription, pop: true, sender: self)
                    } else {
                        self.refreshControl?.endRefreshing()
                    }
                    
                }
                
                if(!(self.refreshControl?.refreshing)!){
                    self.activityIndicator.stopAnimating()
                }
        }
    }

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        if vendorsArray.count > 0
        {
            return 1
        }
            return 0
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return vendorsArray.count
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCellWithIdentifier("cardviewcell", forIndexPath: indexPath) as! CardViewCell
        cell.selectionStyle = .None
        let vendors = vendorsArray[indexPath.row] as! VendorsEntity
        
        let vendorimage = vendors.vendor_image_name! as String
        let imagePath = "\(vendorImageUrl)\(vendorimage)"
        
        let imageURL : NSURL = NSURL(string: imagePath)!
        
        cell.cellLabel.text = vendors.vendor_name! as String
        if(!fromDB){
            cell.cellImage.image = nil
            Alamofire.request(.GET, imageURL)
                    .responseImage{ response in
                        if let image = response.result.value
                        {
                            cell.cellImage.image = image
                            self.saveImage(image,image_name: vendors.vendor_image_name!)
                        }
                        else
                        {
                            cell.cellImage.image = UIImage(named: "no_image")
                        }
                        cell.cellActivityIndicator.stopAnimating()
                        cell.cellActivityIndicator.hidesWhenStopped = true
            }
        } else {
                let file = fileInDocumentsDirectory(vendors.vendor_image_name!)
                let fileManager = NSFileManager.defaultManager()
            if(fileManager.fileExistsAtPath(file)){
                let imageContent = UIImage(contentsOfFile: file)
                if(imageContent != nil){
                    cell.cellImage.image = imageContent
                } else {
                    cell.cellImage.image = UIImage(named: "no_image")
                }
            } else {
                cell.cellImage.image = UIImage(named: "no_image")
                }
            }
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        vendorDetail = vendorsArray[indexPath.row] as! VendorsEntity
        self.performSegueWithIdentifier("vendorsdetail", sender: nil)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?)
    {
        let vendorObj: VendorsDetail = segue.destinationViewController as! VendorsDetail
        vendorObj.vendorDetail = vendorDetail
        vendorObj.fromDB = true
    }
    
    func getVendorsDataFromDB(){
        let vendors = DatabaseHandler(entityName: "VendorsEntity")
        vendorsArray = vendors.getVendorsData(self.vendortypeId)
    }
    
    func handleRefresh(refreshControl:UIRefreshControl){
        fromDB = false
        getVendorsData()
    }
    
    
    func saveImage(image:UIImage,image_name:String){
        if let data = UIImagePNGRepresentation(image){
            let filename = getDocumentsDirectory().stringByAppendingPathComponent(image_name)
            data.writeToFile(filename, atomically: true)
        }
    }
    
    func getDocumentsDirectory() -> NSString{
        let paths = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }
    
    func fileInDocumentsDirectory(fileName:String) -> String {
        let fileName = getDocumentsDirectory().stringByAppendingPathComponent(fileName)
        return fileName
    }
}
